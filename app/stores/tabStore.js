import { observable, action } from 'mobx';

class TabStore {
  @observable productEeastside = {
    defaultTab: 'map',
    activeTab: ''
  }

  @action
  changeTab = (tabGroupName, activeTab) => {
    if (this[tabGroupName]) {
      this[tabGroupName].activeTab = activeTab;
    }
  }
}

const tabStore = new TabStore();

export default tabStore;
export { TabStore };
