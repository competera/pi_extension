import { observable, action, computed } from 'mobx';
import { getMarketChartSerie } from 'utils/chart';
import _ from 'lodash';

const removeApi = str => str.replace(/\/api\/v\d\//, '');  // removes /api/v1/ prefix
const calculateDiff = (storePrice, ourPrice) => ourPrice
  ? _.round(((storePrice - ourPrice) / ourPrice) * 100, 2)
  : null;

class ProductStore {
  @observable product = {};
  @observable stores = [];

  @observable scans = [];


  @action
  saveProduct(product, stores) {
    this.product = product;
    this.stores = stores.map(store => ({
      ...store,
      scans: [],
      notMatched: !store.id,
      notFound: !!store.id && _.get(store, 'scan.warning') === 'no_match',
      violatesMAP: store.scan.in_stock && store.scan.price && store.scan.price < product.map
    }));
  }

  @action
  saveMyPriceHistory(scans) {
    this.scans = scans;
  }

  @action
  saveStoresPriceHistory(priceHistories) {
    this.stores.forEach(store => {
      const storeHistory = priceHistories
        .find(history => store.resource_uri && removeApi(history.resource_uri) === removeApi(store.resource_uri));
      store.scans = _.get(storeHistory, 'scans', []);
    })
  }

  @computed
  get marketChartSerie() {
    return getMarketChartSerie(this.scans, this.stores);
  }

  @computed
  get getStores() {
    if (!this.stores.length) return [];

    const { resource_uri: productUrl, scan, id: productId, map } = this.product;

    return this.stores.map(store => {
      const productPrice = scan.price;
      const price = store.scan.price || store.scan.last_in_stock_price;

      const priceDiff = calculateDiff(price, productPrice);
      const mapDiff = calculateDiff(price, map);

      const hasPriceHistory = store.scans.some(scan => scan.in_stock);
      const violatesMAP = store.scan.in_stock && store.scan.price && store.scan.price < map;

      let href = '';
      if (store.url) // if store is matched
        href = `https://dashboard.competera.net/campaign/${1026}/go_product_url/${productId}/${store.store.id}/`;


      return {
        id: store.id,
        name: store.store.name,
        href,
        price,
        priceDiff,
        mapDiff,
        lastUpdate: store.scan.check_time,
        map_violation_time: store.scan.map_violation_time,
        productUrl,
        competitorUrl: store.store.resource_uri,
        inStock: store.scan.in_stock,
        notMatched: store.notMatched,
        notFound: store.notFound,
        hasPriceHistory,
        violatesMAP
      }
    });
  }

  @computed
  get matchedStores() {
    return this.getStores.filter(store => !store.notFound && !store.notMatched);
  }

  @computed
  get violatedStores() {
    return this.getStores.filter(store => store.violatesMAP);
  }

  @computed
  get firstViolator() {
    const { map } = this.product;
    if (!map) return null;

    const violator = this.stores.find(store => store.scan.is_first_map_violation);
    return violator && { ...violator, map }
  }


}


const productStore = new ProductStore();

export default productStore;
export { ProductStore };
