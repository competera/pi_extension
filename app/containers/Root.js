import React, { Component } from 'react';
import { Provider } from 'mobx-react';
import * as stores from 'stores';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


import App from 'containers/App';


export default class Root extends Component {

  render() {
    return (
      <Provider {...stores}>
        <MuiThemeProvider>
          <App/>
        </MuiThemeProvider>
      </Provider>
    );
  }
}
