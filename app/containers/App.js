import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classnames from 'classnames';

import { Text, Tab } from 'components/eastside/common';
import {
  EastsideWrapper,
  EastsideHeader,
  EastsideTabNavigation,
  EastsideTabContainer
} from 'components/eastside';

import { MarketTab, PromoTab, MapTab } from 'components/eastside/tabs';

import 'shared/styles/global/_reset.scss';
import 'rc-tooltip/assets/bootstrap.css';

import { fetchProduct } from 'actions';

import styles from './App.scss';

@inject('productStore', 'tabStore')
@observer
export default class App extends Component {
  componentDidMount() {
    fetchProduct();
  }

  render() {
    const { product } = this.props.productStore;
    const { productEeastside, changeTab } = this.props.tabStore;
    const containerClassName = classnames(styles.container, 'theme_price_management');

    return (
      <div className={containerClassName}>
        <EastsideWrapper
          tabGroupName={'productEeastside'}
          activeTab={productEeastside.activeTab}
          defaultTab={productEeastside.defaultTab}
          onChangeTab={changeTab}
        >
          <EastsideHeader>
            <Text text={product && product.title} type={'title'} />
            <Text text={product && product.sku} type={'info'} />

            <EastsideTabNavigation>
              <Tab id={'market'} tabName={'products_eastside_tab_market'} />
              <Tab id={'promo'} tabName={'products_eastside_tab_promo'} />
              <Tab id={'map'} tabName={'products_eastside_tab_map'} />
            </EastsideTabNavigation>
          </EastsideHeader>


          <EastsideTabContainer>
            <MarketTab id={'market'} />
            <PromoTab id={'promo'} />
            <MapTab id={'map'} />
          </EastsideTabContainer>
        </EastsideWrapper>
      </div>
    );
  }
}
