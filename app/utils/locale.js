import en from 'shared/i18n/en.json';
import ru from 'shared/i18n/ru.json';

export const formatMessage = (formattedMessageID, values = {}) => {
  const locale = 'ru';
  if (!formattedMessageID) { return '' }

  const argType = typeof formattedMessageID;
  if (argType !== 'string') {
    if (formattedMessageID) {
      console.error('Formatted Message id should be a [String]: ' + argType) // eslint-disable-line
    }
    return null;
  }
  const isValidFormattedMessageID = formattedMessageID.includes('_') &&
                                    !formattedMessageID.includes(' ');
  const isTranslationMissed = !ru[formattedMessageID] || !en[formattedMessageID];
  if (isValidFormattedMessageID && isTranslationMissed) {
    if (window.location.hostname === 'localhost') {
      let translations = (localStorage.getItem('translations') || '').split(',');
      translations = new Set(translations);
      translations.add(formattedMessageID);

      localStorage.setItem('pi_translations', [...translations].join(','));
    }
  }

  switch (locale) {
    case 'ru': {
      return Object.entries(values).reduce(
        (message, [key, value]) => message.replace(`{${key}}`, value),
        ru[formattedMessageID]
      ) || formattedMessageID;
    }

    default: {
      return Object.entries(values).reduce(
        (message, [key, value]) => message.replace(`{${key}}`, value),
        en[formattedMessageID]
      ) || formattedMessageID;
    }
  }
};

export const formatNumber = (formattedNumber, toFixed = false) => {
  const locale = 'ru';
  const argType = typeof formattedNumber;
  if (argType !== 'string' && argType !== 'number') {
    if (formattedNumber) {
      console.error('Formatted number should be a [String] or [Number]: ' + argType) // eslint-disable-line
    }
    return null;
  }

  const numberSign = Number(formattedNumber) < 0 ? '-' : '';
  const numberAsString = (formattedNumber == 0 || formattedNumber) && ( // eslint-disable-line
    toFixed
      ? parseFloat(formattedNumber).toFixed(2)
      : _.round(formattedNumber, 3).toString()
  );
  const [integerNumber, decimalNumber] = numberAsString.replace(',', '.').split('.');

  const leftNumberPartIndex = integerNumber.length % 3;
  const leftNumberPart = integerNumber.slice(0, leftNumberPartIndex);
  const rightNumberPart = integerNumber.slice(leftNumberPartIndex);
  const integerNumberArray = [];

  if (leftNumberPart) {
    integerNumberArray.push(leftNumberPart);
  }
  integerNumberArray.push.apply(integerNumberArray, rightNumberPart.match(/.{1,3}/g)); // eslint-disable-line

  switch (locale) {
    case 'ru': {
      return numberSign +
        integerNumberArray.join(',') +
        (decimalNumber ? `.${decimalNumber}` : '');
    }
    default: {
      return numberSign +
        integerNumberArray.join(',') +
        (decimalNumber ? `.${decimalNumber}` : '');
    }
  }
};



