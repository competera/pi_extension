import { normalizeDate, durations } from 'utils/timing';
import moment from 'moment';
import _ from 'lodash';


const defaultObject = {};
const day = 24 * 3600 * 1000; // one day

const blackLine = '#6B7176', grayLine = '#AFB5BA';
const monthAgo = moment().subtract(30, 'day');
const daysArray = [...Array(90).keys()].reverse().map(day => moment().startOf('day').subtract(day, 'day'));


const getInterpolated = data => {
  const result = [];
  daysArray.forEach(date => {
    const interpolated = data[date] || _.last(result) || defaultObject;
    result.push({ ...interpolated, date });
  });
  return result;
};

const getReducedData = scans => {
  if (scans.length) {
    const reduced = scans.reduce((acc, scan) => {
      const date = moment(scan.timestamp).startOf('day');
      return ({ ...acc, [date]: { ...scan } });
    }, {});

    const interpolated = getInterpolated(reduced);
    return interpolated
      .filter(scan => scan.date && scan.date > monthAgo)
      .map((scan => {
      return {
        x: scan.date.valueOf(),
        y: scan.in_stock ? scan.price : null,
      }
    }))
  }
  return []
};

const getReducedStores = stores => stores.reduce((acc, { scans = [], store }) => ({
  ...acc,
  [store.name]: getReducedData(scans)  // key - storeName, value - priceChange object
}), {});


const getStoresPricesByDate = (stores, idx) => Object.entries(stores)
  .map(([storeName, scans]) => _.nth(scans, idx))
  .filter(scan => scan && scan.y)
  .sort((a, b) => a.y - b.y);


const getMyPriceSerie = scans => {
  if (!scans.length) return
  return {
    type: 'line',
    name: 'my_price',
    color: blackLine,
    pointInterval: day,
    data: getReducedData(scans)
  }
};


const getMinMaxSerie = stores => {

  if (!stores.length || !stores[0].scans) return
  const reduced = getReducedStores(stores);
  const dates = daysArray.filter(date => date >= monthAgo);

  const data = dates.map((date, idx) => {
    const prices = getStoresPricesByDate(reduced, idx);
    const storesStock = prices.length;
    const low = _.get(prices, '[0].y', null), high = _.get(prices, `[${prices.length - 1}].y`, null);
    return { x: date.valueOf(), low, high, storesStock }
  });

  return {
    type: 'areasplinerange',
    name: 'minmax',
    pointInterval: day,
    data
  }
};


export const getMarketChartSerie = (scans, stores) => {
  const myPriceSerie = getMyPriceSerie(scans);
  const minmaxSerie = getMinMaxSerie(stores);

  return [myPriceSerie, minmaxSerie].filter(serie => serie);
};

