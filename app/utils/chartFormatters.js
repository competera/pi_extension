import { formatNumber, formatMessage } from 'utils/locale';
import Highcharts from 'highcharts/highstock'
const green = '#1bb35c', red = '#e71d32', gray = '#8fa2ac';


const getPriceHTML = (price, color = gray) => {
  const [integer, divider, fraction] = formatNumber(parseFloat(price), true).split(/(\.|,\d\d$)/);

  return (
    `<span style='font-size: 13px;'>
        <span style='color:${color};'>${integer}</span><span style='color:#8fa2ac;'>${divider}${fraction}</span>
    </span>`
  )
}


const createProductTooltipLine = point => `
      <div style='font-size: 12px;color: #6b7176;display: flex;justify-content: space-between;'>
          <span style='padding-right: 20px'>
              <span style='color:${point.color}; visibility: ${point.color ? 'visible' : 'hidden'}'>
                \u25CF
              </span>
              ${point.storeName}
          </span>
          <span>
          ${'price' in point
  ? getPriceHTML(point.price, point.priceColor)
  : point.y || ''
  }
          </span>
      </div>
     `

export const priceChartFormatter = function () {
  let s = `<div style='font-size: 12px;color: #6b7176;margin: 0 0 10px 10px;'>
            ${Highcharts.dateFormat('%A, %b %e', this.x)}
            </div>`;
  let offers = 0;
  const points = [];

  const columnSerie = this.points.find(point => point.series.name === 'minmax');
  if (columnSerie) {
    points.push(
      { price: columnSerie.point.high, storeName: formatMessage('maximum_price') },
      { price: columnSerie.point.low, storeName: formatMessage('minimum_price') }
    );
    offers = columnSerie.point.storesStock;
  }
  const mySerie = this.points.find(point => point.series.name === 'my_price');
  if (mySerie) {
    points.push({ price: mySerie.y, storeName: formatMessage('chart_my_price'), priceColor: green })
  }

  const storeSerie = this.points.find(point => point.series.name === 'store_price');
  if (storeSerie) {
    points.push(({ price: storeSerie.y, storeName: formatMessage('chart_store_price'), priceColor: red }));
  }

  points.push({ y: offers, storeName: formatMessage('chart_offers') });
  return s + points.map(point => createProductTooltipLine(point)).join('');
};

