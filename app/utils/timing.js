export const hour = 60 * 60 * 1000;
export const day = 24 * hour;
export const week = 7 * day;
export const month = 30 * day;
export const year = 365 * day;
export const getDays = date => Math.floor(date / day);
export const normalizeDate = (date, interval) => Math.floor(date / interval);

export const durations = {
  hour,
  day,
  week,
  month,
  quarter: 3 * month,
  half_year: 6 * month,
  year: 12 * month
};


