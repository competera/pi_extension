export const checkPlatformIsMac = () => navigator.platform.toLowerCase().indexOf('mac') >= 0

export const isKeyPressed = (event) => {
  return {
    ctrl: checkPlatformIsMac() ? event.metaKey : event.ctrlKey,
    shift: event.shiftKey
  }
}
