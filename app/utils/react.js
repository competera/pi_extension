import React from 'react';

const defaultShouldRender = () => true;

export const renderChildrenWithProps = (children, props, shouldRender = defaultShouldRender) => // eslint-disable-line
  children && Array.isArray(children)
    ? React.Children.map(
	    	children,
	    	child => child && shouldRender(child, props) && React.cloneElement(child, props)
	    )
    : shouldRender(children, props) && React.cloneElement(children, props);
