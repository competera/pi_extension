import React from 'react'
import { formatNumber } from 'utils/locale'

import styles from './price.scss'
import classnames from 'classnames'

const Price = ({ cost, color = '', fontSize = 13 }) => {
  if (typeof cost !== 'number' && typeof cost !== 'string') return false
  const [integer, delimiter, fraction] = formatNumber(parseFloat(cost), true).split(/(\.|,\d\d$)/)

  const containerStyle = { fontSize }
  const integerClassName = classnames(styles.integer, styles[color])

  return (
    <span style={containerStyle}>
      <span className={integerClassName}>
        {integer}
      </span>

      <span className={styles.fraction}>
        {`${delimiter}${fraction}`}
      </span>
    </span>
  )
}

export default Price
