import React, { Component } from 'react';
import Highcharts from 'highcharts/highstock';
import HighchartsMore from 'highcharts-more';
HighchartsMore(Highcharts);
// import { ruLocale } from 'tools/chart'
// import { getLocale } from 'core/selectors/router'
import styles from './PriceChart.scss';
// import { priceChartFormatter } from 'tools/chart'




export default class PriceChart extends Component {
  initChart = props => {
    const { series }  = props;
    const ref = this.refs.chart;

    this.chart && this.chart.destroy();
    this.chart = new Highcharts.Chart({
      chart: {
        renderTo: ref,
        height: 200,
        width: 435,
        plotBackgroundColor: '#fafafa',
        plotBorderWidth: 1,
        plotBorderColor: '#dfe3e6',
        className: styles.chart
      },
      credits: {
        enabled: false
      },
      title: { text: '' },
      exporting: { enabled: false },
      legend: { enabled: false },
      tooltip: {
        shared: true,
        followPointer: false,
        useHTML: true,
        formatter: props.formatter
      },
      xAxis: {
        type: 'datetime',
        title: { text: '' },
        crosshair: true
      },
      yAxis: {
        title: { text: '' },
        opposite: false,
      },
      plotOptions: {

        areasplinerange: {
          color: 'rgba(143, 162, 172, 0.5)',
          animation: {
            duration: 800
          },
        },
        line: {
          marker: {
            enabled: false
          },
          animation: {
            duration: 800
          },
        }
      },
      series
    });
  };


  componentDidMount() {
    this.initChart(this.props);
  }

  componentWillMount() {
    // if (this.props.locale === 'ru') {
    //   Highcharts.setOptions({ lang: ruLocale })
    // }
  }

  componentWillReceiveProps(nextProps) {
    this.initChart(nextProps);
  }


  render() {
    const { series } = this.props;
    const containerClassName = series.length ? '' : styles.hidden;

    return (
      <div>
        <div ref='chart'></div>
      </div>
    )
  }
}


