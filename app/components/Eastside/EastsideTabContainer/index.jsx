import React from 'react';
import { renderChildrenWithProps } from 'utils/react';
import { observer } from 'mobx-react';

import styles from './EastsideTabContainer.scss';

const shouldRenderTabContainer = (tab, props) => {
  const activeOrDefaultTab = props.activeTab.length > 0 ? props.activeTab : props.defaultTab;
  return tab.props.id === activeOrDefaultTab;
};

const EastsideTabContainer = observer(({ children, ...props }) => 
  <div className={styles.container}>
    {renderChildrenWithProps(children, props, shouldRenderTabContainer)}
  </div>
);

export default EastsideTabContainer;
