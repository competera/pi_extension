import React from 'react';
import { renderChildrenWithProps } from 'utils/react';
import { observer } from 'mobx-react';

import styles from './EastsideHeader.scss';

const EastsideHeader = observer(({ children, ...props }) =>
  <div className={styles.container}>
    {renderChildrenWithProps(children, props)}
  </div>
);

export default EastsideHeader;
