import React from 'react';
import { inject, observer } from 'mobx-react';
import PriceChart from 'components/Eastside/common/PriceChart';
import { getPriceHistorySeries } from 'utils/chart';
import StoresTable from './StoresTable'
import EastsideBlock from 'components/Eastside/EastsideBlock';
import { priceChartFormatter } from 'utils/chartFormatters';


@inject('productStore')
@observer
class Market extends React.Component {


  showHistory = (id, priceHistoryTop) => {
    this.setState({ priceHistoryTop }, () => this.props.showHistory(id))
  };


  render() {
    const { marketChartSerie, matchedStores } = this.props.productStore;

    return (
      <div>
        <EastsideBlock titleId='our_price_history' isNoPadding>
          <PriceChart series={marketChartSerie} formatter={priceChartFormatter} />
        </EastsideBlock>

        {matchedStores.length > 0 && (
          <EastsideBlock titleId='matched_stores' isNoPadding isRow>
            <StoresTable
              stores={matchedStores}
              showHistory={this.showHistory}
            />
          </EastsideBlock>
        )}

      </div>
    )
  }

}

export default Market
