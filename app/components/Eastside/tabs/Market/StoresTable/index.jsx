import React from 'react';
import FDTable from 'components/fd_table';
import { formatMessage } from 'utils/locale';
import StoresDropdown from './stores_dropdown';
import Price from 'components/Price';

import moment from 'moment';



export default  class StoresTable extends React.Component {
  constructor() {
    super();

    this.columns = [
      {
        key: 'name',
        header: formatMessage('products_table_header_competitor_cell'),
        defaultSorted: true,
        width: 100,
        sortable: true,
      },
      {
        key: 'price',
        header: formatMessage('products_eastside_price'),
        sortable: true,
        align: 'right',
        width: 90,
        renderer: price => <Price cost={price} />
      },
      {
        key: 'lastUpdate',
        header: formatMessage('last_update'),
        sortable: true,
        align: 'right',
        width: 90,
        renderer: lastUpdate => {
          return moment(lastUpdate).toNow(true)
        }
      },
      {
        key: 'priceDiff',
        header: formatMessage('products_eastside_to_our_price'),
        align: 'right',
        width: 90,
        sortable: true,
        renderer: value => `${value}%`
      },
      {
        key: 'dropdown',
        header: ' ',
        width: 45,
        noPadding: true,
        renderer: (_, row) => <StoresDropdown store={row} />
      }
    ]
  }


  render() {
    const { stores } = this.props;

    return (
      <FDTable
        data={stores}
        columns={this.columns}
      />
    )
  }
}







































