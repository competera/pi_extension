import React from 'react'
import { IconDropdown } from 'components/Dropdown'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import Divider from 'material-ui/Divider'
import { formatMessage } from 'utils/locale'
// import ReportMenuItem from 'components/ReportMenuItem'
import _ from 'lodash'


const MoreVert = <MoreVertIcon color='#8fa2ac' />


export default class StoresDropdown extends React.Component {
  static defaultProps = {
    menuItems: ['data-lag', 'invalid-link', 'invalid-price', 'invalid-availability', 'invalid-promotion', 'other'],
  }


  openNewTab = () => {
    const { store: { href } } = this.props
    window.open(href, '_blank')
  }

  showPriceHistory = () => {
    const top = _.get(this.dropdown.getBoundingClientRect(), 'top', 0)
    this.props.showHistory(this.props.store.id, top - 50)
  }

  render() {
    const { store } = this.props

    return (
      <div ref={dropdown => this.dropdown = dropdown}>
        <IconDropdown
          align={'right'}
          width={300}
          icon={MoreVert}
        >
            <MenuItem
              primaryText={formatMessage('global_go_to')}
              onClick={this.openNewTab}
            />

          {store.hasPriceHistory &&  (
            <MenuItem
              primaryText={formatMessage('price_change_history')}
              onClick={this.showPriceHistory}
            />
          )}

        </IconDropdown>
      </div>
    )
  }
}
