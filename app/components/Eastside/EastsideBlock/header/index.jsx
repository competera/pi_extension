import React from 'react'

import styles from './eastside_block_header.scss'

const EastsideBlockHeader = ({children}) => {
  return (
    <header className={styles.container}>
      {children}
    </header>
  )
}

export default EastsideBlockHeader
