import React, { PropTypes, Component } from 'react'
import classNames from 'classnames'
import { formatMessage } from 'utils/locale'
import styles from './eastside_block.scss'

export EastsideBlockHeader from './header'



export default class EastsideBlock extends Component {
  render() {
    const {
      children,
      isDisplayed=true,
      isNoPadding,
      titleId
    } = this.props

    const containerClass = classNames(
      styles.container,
      isNoPadding && styles.noPadding
    )

    if (!isDisplayed) return null

    return (
      <div className={styles.wrapper}>
        {titleId && (
          <div className={styles.blockHeader}>
            {formatMessage(titleId)}
          </div>
        )}
        <div className={containerClass}>
          {children}
        </div>
      </div>
    )
  }
}
