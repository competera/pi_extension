import React from 'react';
import { renderChildrenWithProps } from 'utils/react';
import { observer } from 'mobx-react';

import style from './EastsideWrapper.scss';

const EastsideWrapper = observer(({ children, ...props }) => 
  <div className={style.container}>
    {renderChildrenWithProps(children, props)}
  </div>
);

export default EastsideWrapper;
