import React, { PropTypes, Component } from 'react'
import classNames from 'classnames'

import styles from './eastside_navigation.scss'

export default class EastsideNavigation extends Component {
  static propTypes = {
    children: PropTypes.bool,
    onClose: PropTypes.func
  }

  render() {
    const { children, onClose, color } = this.props
    const containerClass = classNames(
      styles.container,
      styles[color]
    )

    return (
      <nav className={containerClass}>
        <button
          className={styles.controlButton}
          onClick={onClose}
        >
          <i className='material-icons'>arrow_forward</i>
        </button>
        {children}
      </nav>
    )
  }
}
