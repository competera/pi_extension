import React from 'react';
import { renderChildrenWithProps } from 'utils/react';
import { observer } from 'mobx-react';

import styles from './EastsideTabNavigation.scss';

const EastsideTabNavigation = observer(({ children, ...props }) => 
  <div className={styles.container}>
    {renderChildrenWithProps(children, props)}
  </div>
);

export default EastsideTabNavigation;
