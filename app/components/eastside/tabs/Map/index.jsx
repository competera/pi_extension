import React  from 'react';
import { inject, observer } from 'mobx-react';
import EastsideBlock from 'components/Eastside/EastsideBlock';
// import { getFirstViolator, getMapTab } from 'core/selectors/products'
import FDTable from 'components/fd_table';
import Price from 'components/Price';
import moment from 'moment';

import { formatMessage } from 'utils/locale';
import styles from './map.scss';



// const mapStateToProps = state => ({
//   violator: getFirstViolator(state),
//   stores: getMapTab(state)
// })

const Divider = () => <div className={styles.divider}></div>


@inject('productStore')
@observer
class MAPTab extends React.Component {

  constructor() {
    super()
    this.columns = [
      {
        key: 'name',
        header: formatMessage('products_table_header_competitor_cell'),
        defaultSorted: true,
        width: 100,
        sortable: true,
      },
      {
        key: 'map_violation_time',
        header: formatMessage('map_violation_time'),
        sortable: true,
        align: 'right',
        notCropped: true,
        width: 90,
        renderer: time => {
          return moment(time).toNow(true)
        }
      },
      {
        key: 'lastUpdate',
        header: formatMessage('last_update'),
        sortable: true,
        align: 'right',
        notCropped: true,
        width: 90,
        renderer: lastUpdate => {
          return moment(lastUpdate).toNow(true)
        }
      },
      {
        key: 'price',
        header: formatMessage('products_eastside_price'),
        sortable: true,
        notCropped: true,
        align: 'right',
        width: 90,
        renderer: price => <Price cost={price} />
      },
      {
        key: 'mapDiff',
        header: formatMessage('to_map'),
        notCropped: true,
        width: 70,
        sortable: true,
        renderer: value => `${value}%`
      },
    ]
  }

  renderFirstViolator = () => {
    const { firstViolator: violator } = this.props.productStore;
    const violationTime = moment(violator.scan.map_violation_time);

    return (
      <EastsideBlock
        titleId='map_positioning'
      >
        <div className={styles.violatorContainer}>

          <div className={styles.block}>
            <div>
              <Price cost={violator.map} fontSize={18} />
            </div>
            <div className={styles.underBlock}>
              {formatMessage('map')}
            </div>
          </div>
          <Divider />

          <div className={styles.block}>
            <div>{violator.store.name}</div>
            <div className={styles.underBlock}>
              {formatMessage('first_violator')}
            </div>
          </div>
          <Divider />

          <div className={styles.block}>
            <div>{violationTime.format('hh:mm')}</div>
            <div className={styles.underBlock}>
              {violationTime.format('DD MMM YYYY')}
            </div>
          </div>


        </div>
      </EastsideBlock>
    )
  };

  render() {

    const { firstViolator, violatedStores } = this.props.productStore

    return (
      <div>
        {firstViolator && this.renderFirstViolator()}

        <EastsideBlock titleId='map_violations' isNoPadding>
          <FDTable
            data={violatedStores}
            columns={this.columns}
          />
        </EastsideBlock>
      </div>
    )
  }
}

export default MAPTab
