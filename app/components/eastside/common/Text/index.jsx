import React from 'react';
import classnames from 'classnames';
import CroppedText from 'components/CroppedText';

import { formatMessage } from 'utils/locale';
import { observer } from 'mobx-react';

import styles from './Text.scss';

const Text = observer(({ text, values, type }) => {
  const containerClass = classnames(styles.container, styles[type]);

  return (
    <div className={containerClass}>
      <CroppedText>
        {formatMessage(text, values)}
      </CroppedText>
    </div>
  );
});

export default Text;
