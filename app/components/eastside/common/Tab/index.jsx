import React, { Component } from 'react';
import classNames from 'classnames';
import { formatMessage } from 'utils/locale';

import { observer } from 'mobx-react';

import styles from './Tab.scss';

@observer
export default class Tab extends Component {
  onTabClick = (event) => {
    event.preventDefault();
    const { id, tabGroupName, onChangeTab } = this.props;
    onChangeTab(tabGroupName, id);
  }

  render() {
    const { id, activeTab, defaultTab, tabName } = this.props;
    const activeTabOrDefault = activeTab.length > 0 ? activeTab : defaultTab;
    const isActive = activeTabOrDefault === id;
    const containerClass = classNames(
      styles.container,
      isActive && styles.active
    );

    return (
      <button onMouseDown={this.onTabClick} className={containerClass}>
        {formatMessage(tabName)}
      </button>
    );
  }
}
