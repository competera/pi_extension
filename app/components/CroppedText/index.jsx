import React, { Component } from 'react';
import classnames from 'classnames';
import Tooltip from 'rc-tooltip';

import styles from './CroppedText.scss';

export default class CroppedText extends Component {
  static defaultProps = {
    overlayPlacement: 'topLeft'
  };

  state = {
    shouldDisplayTooltip: false
  };

  componentDidMount() {
    const { overlayShown } = this.props;
    const { offsetWidth, scrollWidth } = this.wrapper;
    const shouldDisplayTooltip = !!overlayShown || offsetWidth !== scrollWidth;
    this.setState({ shouldDisplayTooltip }); // eslint-disable-line
  }

  componentDidUpdate(prevProps, prevState) {
    const { overlayShown } = this.props;
    const { offsetWidth, scrollWidth } = this.wrapper;
    const shouldDisplayTooltip = !!overlayShown || offsetWidth !== scrollWidth;

    if (prevState.shouldDisplayTooltip !== shouldDisplayTooltip) {
      this.setState({ shouldDisplayTooltip }); // eslint-disable-line
    }
  }

  render() {
    const { className, overlay, children, overlayPlacement } = this.props;
    const { shouldDisplayTooltip } = this.state;
    const overlayCroppedText = overlay || children || '';
    const containerClassMerged = classnames(
      styles.container,
      className
    );

    return (
      <Tooltip
        placement={overlayPlacement}
        overlay={overlayCroppedText}
        shouldDisplayTooltip={shouldDisplayTooltip}
      >
        <span className={containerClassMerged} ref={wrapper => this.wrapper = wrapper}>
          {children}
        </span>
      </Tooltip>
    );
  }
}
