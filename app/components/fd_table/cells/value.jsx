import React from 'react'
import classnames from 'classnames'
import CroppedText from 'components/CroppedText'

import styles from './value.scss'

const Value = ({ text, color, align, isActive = true }) => {
  const containerClass = classnames(
    styles.container,
    color && styles[color],
    align && styles[align],
    !isActive && styles.inactive
  )

  return (
    <div className={containerClass}>
      <CroppedText className={styles.text}>{text}</CroppedText>
    </div>
  )
}

export default Value
