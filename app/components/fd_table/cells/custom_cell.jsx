import React from 'react'
import { Cell } from 'fixed-data-table-2'
import classnames from 'classnames'
import styles from './custom_cell.scss'
import _ from 'lodash'
import CroppedText from 'components/CroppedText'
import { formatNumber } from 'utils/locale'

export default class CustomCell extends React.Component {

  renderValue = value => {
    const { notCropped, formatted, toFixedNumber } = this.props
    const displayedValue = formatted ? formatNumber(value, toFixedNumber) : value

    return notCropped
      ? <span>{displayedValue}</span>
      : <CroppedText>{displayedValue}</CroppedText>
  }

  render() {
    const { data, rowIndex, columnKey, renderer, align, noPadding } = this.props
    const row = data[rowIndex]
    const value = _.get(row, columnKey)
    const cellClassName = classnames(
      styles.cell,
      align==='right' && styles.alignRight,
      noPadding && styles.noPadding
    )

    const contentClassName = classnames(
      styles.content,
      noPadding && styles.noPadding
    )

    return (
      <Cell className={cellClassName}>
        <div className={styles.wrapper}>
          <div className={contentClassName}>
            {renderer
              ? renderer(value, row, data)
              : this.renderValue(value)
            }
          </div>
        </div>
      </Cell>
    )
  }
}
