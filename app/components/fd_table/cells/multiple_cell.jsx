import React from 'react'
import { Cell } from 'fixed-data-table-2'
import classnames from 'classnames'
import styles from './multiple_cell.scss'
import _ from 'lodash'
import CroppedText from 'components/CroppedText'
import { formatNumber} from 'utils/locale'

export default class MultipleCell extends React.Component {

  renderValue = value => {
    const { notCropped, formatted, toFixedNumber } = this.props
    const displayedValue = formatted ? formatNumber(value, toFixedNumber) : value

    return notCropped
      ? <span>{displayedValue}</span>
      : <CroppedText>{displayedValue}</CroppedText>
  }

  render() {
    const {
      data,
      rowIndex,
      renderer,
      rowHeight,
      align,
      noPadding,
      arrayKey = '',
      arrayData,
    } = this.props

    const row = data[rowIndex]
    const items = _.get(row, arrayData)
    const cellClassName = classnames(
      styles.cell,
      align==='right' && styles.alignRight,
      noPadding && styles.noPadding
    )
    const cellStyle = { height: rowHeight }

    return (
      <Cell className={cellClassName}>
        <div className={styles.wrapper}>
          <div className={styles.content}>
            {items.map((item, idx) => {
              const value = _.get(item, arrayKey)
              return (
                <div className={styles.multipleCell} style={cellStyle} key={idx}>
                  {renderer
                    ? renderer(value, row, data)
                    : (this.renderValue(value))
                  }
                </div>)
            })}
          </div>
        </div>
      </Cell>
    )
  }
}
