// import React from 'react'
// import classnames from 'classnames'
// import { formatMessage } from 'tools/locale'
// import CroppedText from 'components/CroppedText'
// // import Tooltip from 'components/tooltip/rc_tooltip'
//
// import styles from './list_of_values.scss'
//
// const ListOfValues = ({values, isActive = true}) => {
//   const isTooManyValues = values.length > 3
//   const andMore = values.length - 2
//   const slicedValues = isTooManyValues ? values.slice(0, 2) : values
//   const otherValues = values.slice(2)
//   const containerClass = classnames(
//     styles.container,
//     !isActive && styles.inactive
//   )
//   return (
//     <div className={containerClass}>
//       {
//         slicedValues.map(value => (
//           <CroppedText key={value} className={styles.item}>{value}</CroppedText>
//         ))
//       }
//       <Tooltip
//         placement={'bottomLeft'}
//         overlay={otherValues.join(', ')}
//         shouldDisplayContent={isTooManyValues}
//       >
//         <span className={styles.andMore}>{formatMessage('fd_table_and_more', { value: andMore })}</span>
//       </Tooltip>
//     </div>
//   )
// }
//
// export default ListOfValues
