import React from 'react'
import { Cell } from 'fixed-data-table-2'
import classnames from 'classnames'
import styles from './header_cell.scss'
import ArrowUpward from 'material-ui/svg-icons/navigation/arrow-upward'
import ArrowDownward from 'material-ui/svg-icons/navigation/arrow-downward'
import CroppedText from 'components/CroppedText'

const svgStyle = {
  height: '100%',
  width: '100%',
}


export default class HeaderCell extends React.Component {
  _onSortChange = e => {
    e.preventDefault()
    const { onSortChange, columnKey } = this.props
    onSortChange && onSortChange(columnKey)
  }

  renderSortIcon = () => {
    const { sortKey, order, columnKey } = this.props
    let sortIcon
    if (sortKey === columnKey)
      sortIcon = order === 'asc' ?
        (<ArrowUpward color='rgba(0,0,0,87)' style={svgStyle} />)
        : (<ArrowDownward color='rgba(0,0,0,87)' style={svgStyle}  />)
    else
      sortIcon = <ArrowUpward color='transparent' style={svgStyle}  />
    return (
      <div className={styles.iconWrapper}>
        {sortIcon}
      </div>
    )
  }

  render() {
    const { children, align, sortable, columnKey, sortKey, noPadding, notCropped } = this.props
    const isActive = sortable && columnKey === sortKey
    let leftSortIcon, rightSortIcon
    if (sortable && align === 'right')
      leftSortIcon = this.renderSortIcon()
    else if (sortable && align === 'left')
      rightSortIcon = this.renderSortIcon()

    const cellClassName = classnames(styles.container,
      align === 'right' && styles.alignRight,
      noPadding && styles.noPadding
    )

    const content = notCropped
      ? <span>{children}</span>
      : <CroppedText className={styles.text}>{children}</CroppedText>

    return (
      <Cell className={cellClassName}>
        <div className={styles.wrapper}>
          <div className={styles.content}>
            {sortable
              ? (
              <a
                onClick={this._onSortChange}
                className={classnames(styles.sortedHeader, isActive && styles.active)}
              >
                {leftSortIcon}
                {content}
                {rightSortIcon}
              </a>
              )
              : content
            }
          </div>
        </div>
      </Cell>
    )
  }

}
