import React from 'react'
import { Table, Column } from 'fixed-data-table-2'
import HeaderCell from './cells/header_cell'
import CustomCell from './cells/custom_cell'
import MultipleCell from './cells/multiple_cell'
import './cells/header_cell.scss'
import _ from 'lodash'
import { isKeyPressed } from 'utils/keys'
import styles from './fd_table.scss'
import classnames from 'classnames'
import 'shared/styles/table.css'


const reverseSortOrder = order => order === 'asc' ? 'desc' : 'asc'
const generateDefaultIndexes = length => Array.apply(null, { length }).map(Number.call, Number)


export default class FDTable extends React.Component {
  static defaultProps = {
    data: [],
    rowHeight: 50,
    headerHeight: 50,
    multiSelectable: false,
    noPadding: false,
    selectedRows: new Set(),
    joinRowsByKey: undefined,
    tableMaxHeight: null
  }

  constructor(props) {
    super(props)
    const defaultSorted = props.columns.find(col => col.defaultSorted)
    const sortKey = defaultSorted ? defaultSorted.key : ''
    this.state = {
      width: 300,
      order: 'asc',
      sortKey,
      sortedIndexes: this.getSortedIndexes(props.data, sortKey),
      columnsWidth: this.getColumnsWidth(props)
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setTableDimensions()
    this.setState({
      sortedIndexes: this.getSortedIndexes(nextProps.data, this.state.sortKey),
      columnsWidth: this.getColumnsWidth(nextProps)
    })
  }

  componentDidMount() {
    this.setTableDimensions()
    window.addEventListener('resize', this.setTableDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.setTableDimensions)
  }


  setTableDimensions = () => {
    if (this.refs.container) {
      const { offsetWidth } = this.refs.container
      this.setState({ width: offsetWidth }) // eslint-disable-line
    }
  }

  getColumnsWidth = props => {
    return props.columns.reduce((acc, curr) => ({ ...acc, [curr.key]: curr.width || 130 }), {})
  }



  getRowIndex = index => {
    const { sortedIndexes, order } = this.state
    const size = this.props.data.length
    if (order === 'asc')
      return sortedIndexes[index]
    return sortedIndexes[size - index - 1]
  }

  getSortedIndexes = (data, sortKey) => {
    const defaultIndexes = generateDefaultIndexes(data.length)
    if (!sortKey) return defaultIndexes
    return [...defaultIndexes].sort((indexA, indexB) => {
      let valueA = _.get(data[indexA], sortKey)
      let valueB = _.get(data[indexB], sortKey)
      if (typeof valueA === 'string' && typeof valueB === 'string') {
        valueA = valueA.toLowerCase()
        valueB = valueB.toLowerCase()
      }
      if (valueA > valueB)
        return 1
      if (valueA < valueB)
        return -1
      return 0
    })
  }


  checkRowSelection = index => {
    const idx = this.getRowIndex(index)
    const { onRowClick, rowClassNameGetter } = this.props
    return classnames(`fixedDataTable_row_${idx}`,
      !onRowClick && styles.noCursor,
      this.props.selectedRows.has(idx) && 'fixedDataTable_selected_row',
      index % 2 === 0 && 'fixedDataTable_stripped_row',
      rowClassNameGetter && rowClassNameGetter(idx)
    )
  }

  _onSortChange = columnKey => {
    const { data } = this.props
    const { order, sortKey } = this.state
    const nextOrder = reverseSortOrder(order)
    if (columnKey === sortKey)  // simply change ordering, no need for sorting
      this.setState({ order: nextOrder })
    else // sorting
      this.setState({
        order: 'asc',
        sortKey: columnKey,
        sortedIndexes: this.getSortedIndexes(data, columnKey)
      })
  }

  _onRowClick = (e, index) => {
    const { onRowClick, multiSelectable } = this.props
    if (multiSelectable)
      this.toggleRowSelection(e, index)
    else
      onRowClick && onRowClick([this.getRowIndex(index)])
  }

  // need refactoring
  toggleRowSelection = _.debounce((e, index) => {

    const { ctrl, shift } = isKeyPressed(e)
    const { selectedRows: oldSelectedRows, onRowClick } = this.props
    let { sortedIndexes, order } = this.state
    if (order === 'desc')
      sortedIndexes = sortedIndexes.slice().reverse()
    const selectedRows = [...oldSelectedRows].map(row => sortedIndexes.indexOf(row))

    if ((ctrl && shift) || (!ctrl && !shift) || (selectedRows.size === 0)) {
      return onRowClick([this.getRowIndex(index)])
    }
    if (ctrl) {
      const deleted = selectedRows.delete(index)
      return deleted ?
        onRowClick([...selectedRows].map(row => this.getRowIndex(row))) :
        onRowClick([...selectedRows.add(index)].map(row => this.getRowIndex(row)))
    }
    if (shift) {
      const rows = [...selectedRows]
      let last = rows[rows.length - 1]
      let first = rows[0]
      if ((last > index) && rows.includes(index)) {
        while (index < last--) rows.pop()
      }
      else if (last < index) {
        while (last < index) rows.push(++last)
      }
      else if (first > index) {
        while (first > index) rows.unshift(--first)
      }
      return onRowClick(rows.map(row => this.getRowIndex(row)))
    }
  }, 100, { trailing: true })


  renderColumns() {
    const { columns, data, noPadding, rowHeight } = this.props
    const { sortedIndexes, order, sortKey, columnsWidth } = this.state
    const sortedData = sortedIndexes.map(idx => data[idx])
    const orderedData = order === 'asc' ? sortedData : sortedData.slice().reverse()

    return columns.map(column => {
      const cellProps = {
        align: column.align,
        data: orderedData,
        renderer: column.renderer,
        noPadding: column.noPadding || noPadding,
        arrayKey: column.arrayKey,
        arrayData: column.arrayData,
        formatted: column.formatted,
        notCropped: column.notCropped,
        toFixedNumber: column.toFixedNumber,
        rowHeight
      }

      const cell = column.arrayData ? <MultipleCell {...cellProps} /> : <CustomCell {...cellProps} />

      return (<Column
        key={column.key}
        columnKey={column.key}
        fixed={!!column.fixed}
        flexGrow={1}
        width={columnsWidth[column.key]}
        header={
          <HeaderCell
            align={column.align || 'left'}
            order={order}
            sortKey={sortKey}
            sortable={column.sortable}
            noPadding={noPadding}
            onSortChange={this._onSortChange}
            notCropped={column.notCropped}
          >
            {column.header}
          </HeaderCell>
        }
        cell={cell}
      />)
    })
  }

  getTableHeight = () => {
    let { height, maxHeight, data, rowHeight, headerHeight } = this.props
    height = maxHeight ? undefined
      :
      (height || (data.length * rowHeight + headerHeight + 2))  // calculate height if not set
    return { height, maxHeight }
  }

  rowHeightGetter = index => {
    const idx = this.getRowIndex(index)
    const { data, joinRowsByKey, rowHeight } = this.props
    return joinRowsByKey ? data[idx][joinRowsByKey].length * rowHeight : rowHeight
  }


  render() {
    const {
      rowHeight, headerHeight, width,
      data,
      title, onRowDoubleClick, joinRowsByKey
    } = this.props

    const { width: calculatedWidth } = this.state
    const { height, maxHeight } = this.getTableHeight()

    return (
      <div ref='container' className={styles.container}>
        {title && <div className={styles.title}>{title}</div>}
        <div>
          <Table
            width={width || calculatedWidth}
            height={height}
            maxHeight={maxHeight}
            rowHeight={rowHeight}
            rowHeightGetter={joinRowsByKey && this.rowHeightGetter}
            headerHeight={headerHeight}
            rowsCount={data.length}
            onRowClick={this._onRowClick}
            onRowDoubleClick={onRowDoubleClick}
            rowClassNameGetter={this.checkRowSelection}
          >
            {this.renderColumns()}
          </Table>
        </div>
      </div>
    )
  }
}
