import React from 'react'
import { connect } from 'react-redux'
import allMenuItems from './all_menu_items.js'
import Button from 'components/buttons/button.jsx'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import Dialog from 'material-ui/Dialog'
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right'

import { sendErrorReport } from 'core/actions/products'
import { formatMessage } from 'tools/locale'


const arrowStyle = { transform: 'rotate(180deg)', margin: '12px 0 12px 3px' }
const dialogStyle = { zIndex: 10001 } // higher than IconDropdown
const innerDivStyle = { padding: '0 16px 0 34px' }

const ArrowDropLeft = <ArrowDropRight style={arrowStyle} />


const mapDispatchToProps = dispatch => ({
  sendErrorReport: payload => dispatch(sendErrorReport(payload))
})

class ReportMenuItem extends React.Component {
  static defaultProps = {
    target: null,
    menuItems: ['data-lag', 'invalid-link', 'invalid-price', 'invalid-availability', 'invalid-promotion', 'other']
  }

  state = {
    open: false
  }

  _getMenuItems = () => {
    const { menuItems } = this.props
    return allMenuItems
      .filter((menuItem) => menuItems.includes(menuItem.error))
      .map((menuItem) =>
        <MenuItem
          key={menuItem.error}
          primaryText={formatMessage(menuItem.formatMessageId)}
          value={menuItem.error}
          onClick={() => this._onSelectReport(menuItem)}
        />
      )
  }

  _onSelectReport = menuItem => {
    const { product, target, sendErrorReport } = this.props
    const error_type = menuItem.error

    if (error_type === 'other') {
      this._openPopup()
    }
    else {
      const body = {
        product,
        target,
        error_type
      }

      sendErrorReport({ body })
    }
  }

  _openPopup = () => {
    this.setState({ open: true })
  }
  _closePopup = () => this.setState({ open: false, message: '' })

  _onChangeMessageField = (event) => this.setState({ message: event.target.value })

  _sendReportWithMessage = () => {
    const { product, target, sendErrorReport } = this.props
    const { message } = this.state

    const body = {
      product,
      target,
      error_type: 'other',
      message
    }

    sendErrorReport({ body })
    this._closePopup()
  }


  render() {
    const menuItems = this._getMenuItems()

    const actions = [
      <Button id={'button_send'} onClick={this._sendReportWithMessage} />,
      <Button id={'button_cancel'} color={'transparent'} onClick={this._closePopup} />
    ]

    return (
      <MenuItem
        leftIcon={ArrowDropLeft}
        primaryText={formatMessage('products_eastside_tooltip_report_a_problem')}
        menuItems={menuItems}
        innerDivStyle={innerDivStyle}
      >

        <Dialog
          actions={actions}
          open={this.state.open}
          style={dialogStyle}
        >
          <TextField
            hintText={formatMessage('report_button_placeholder')}
            floatingLabelFixed
            floatingLabelFocusStyle={{ color: '#20c968' }}
            floatingLabelText={formatMessage('report_button_label')}
            fullWidth
            multiLine
            rows={1}
            value={this.state.message}
            onChange={this._onChangeMessageField}
            underlineFocusStyle={{ borderColor: '#20c968' }}
          />
        </Dialog>
      </MenuItem>
    )
  }


}

export default connect(null, mapDispatchToProps)(ReportMenuItem)
