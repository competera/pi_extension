const allMenuItems = [
  {
    error: 'data-lag',
    formatMessageId: 'products_eastside_report_data_lag'
  },
  {
    error: 'invalid-link',
    formatMessageId: 'products_eastside_report_invalid_link'
  },
  {
    error: 'invalid-price',
    formatMessageId: 'products_eastside_report_invalid_price'
  },
  {
    error: 'invalid-availability',
    formatMessageId: 'products_eastside_report_invalid_availability'
  },
  {
    error: 'invalid-promotion',
    formatMessageId: 'products_eastside_report_invalid_promotion'
  },
  {
    error: 'other',
    formatMessageId: 'products_eastside_report_other'
  }
]

export default allMenuItems
