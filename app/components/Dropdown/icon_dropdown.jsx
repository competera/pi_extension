import React, { Component } from 'react'
import classnames from 'classnames'
import Popover from 'material-ui/Popover'

import styles from './icon_dropdown.scss'

const initialState = {
  isOpen: false,
  anchorEl: null
}

export default class IconDropdown extends Component {
  state = initialState
  static defaultProps = {
    align: 'right'
  }

  getOrigins = () => {
    const { align } = this.props

    return {
      anchorOrigin: { horizontal: align, vertical: 'bottom' },
      targetOrigin: { horizontal: align, vertical: 'top' }
    }
  }

  openPopover = (event) => {
    this.setState({
      isOpen: true,
      anchorEl: event.currentTarget
    })
  }

  closePopover = () => {
    this.setState({
      isOpen: false,
      anchorEl: null
    })
  }

  render() {
    const { icon, children, align, width, glyph, iconClassName } = this.props
    const { isOpen } = this.state
    const { anchorOrigin, targetOrigin } = this.getOrigins()
    const popoverClass = classnames(
      styles.popover,
      align && styles[align]
    )
    const popoverStyle = { width }

    return (
      <div className={styles.container}>
        <button
          ref={(button) => this.button = button}
          className={styles.container}
          onClick={this.openPopover}
        >
          {
            icon
          }
        </button>
        <Popover
          className={popoverClass}
          style={popoverStyle}
          open={isOpen}
          anchorOrigin={anchorOrigin}
          targetOrigin={targetOrigin}
          anchorEl={this.button}
          onRequestClose={this.closePopover}
        >
          {
            children && Array.isArray(children)
              ? React.Children.map(children, (child) =>
                  child && React.cloneElement(
                    child,
                    { closePopover: this.closePopover }
                  )
              )
              : React.cloneElement(
                  children,
                  { closePopover: this.closePopover }
                )
          }
        </Popover>
      </div>
    )
  }
}
