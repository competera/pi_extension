import React  from 'react'

import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'
import styles from './dropdown.scss'

const menuStyle = { border: '1px solid #dfe3e6' }

const menuItemStyle = { fontSize: 13, color: '#8fa2ac' }
const menuItemInnerDivStyle = { paddingLeft: 10 }

const selectedMenuItemStyle = { color: '#6b7176' }

const listStyle = {
  paddingTop: '0px',
  paddingBottom: '0px',
  overflowX:'hidden'
}

const underlineStyle = { display: 'none' }

const iconStyle = {
  width: 24,
  height: 24,
  padding: 0,
  right: 0
}

export default class Dropdown extends React.Component {
  static defaultProps = {
    dataSourceConfig: {
      text: 'text',
      value: 'value'
    },
    selectedValue: '',
    width: 300,
    anchorOrigin: { vertical: 'bottom', horizontal: 'left' }
  }

  handleChange = (event, index, selectedValue) => this.props.onChange(selectedValue)


  render() {
    const { dataSource, dataSourceConfig, selectedValue, width, anchorOrigin } = this.props
    const dropdownStyle = { width }
    const { value, text } = dataSourceConfig

    return (
      <div>
        <DropDownMenu
          value={selectedValue}
          onChange={this.handleChange}
          anchorOrigin={anchorOrigin}
          style={dropdownStyle}
          menuStyle={menuStyle}
          selectedMenuItemStyle={selectedMenuItemStyle}
          underlineStyle={underlineStyle}
          iconStyle={iconStyle}
          listStyle={listStyle}
          className={styles.dropdown}
          autoWidth={false}
        >
          {dataSource.map(item => {
            return (
              <MenuItem
                value={item[value]}
                primaryText={item[text]}
                innerDivStyle={menuItemInnerDivStyle}
                style={menuItemStyle}
              />
            )
          })}

        </DropDownMenu>
      </div>
    )
  }
}

