import axios from 'axios';
import { productStore } from 'stores'
import moment from 'moment'
import _ from 'lodash'

const domain = 'https://dashboard.competera.net';
const fromDate = moment().subtract(3, 'month').format('YYYY-MM-DD'); // subtract is not deprecated !


export async function fetchProduct(path = 'campaigns/1026/catalog/products/7490125/') {
  try {
    const { data } = await axios.get(`${domain}/api/v2/${path}`);
    const { stores, ...product } = data;
    productStore.saveProduct(product, stores);
    await Promise.all([
      fetchProductPriceHistory(product),
      fetchStoresPriceHistory(stores)
    ])

  } catch (error) {
    console.log(error);
  }
}

async function fetchProductPriceHistory(product) {
  try {
    const { data } = await axios.get(`${domain}/api/v1/campaigns/1026/price_history/?products=${product.id}&from_date=${fromDate}`);
    const scans = _.get(data, 'objects[0].scans', []);
    productStore.saveMyPriceHistory(scans);

  } catch (error) {
    console.log(error);
  }
}


async function fetchStoresPriceHistory(stores) {
  try {
    const storesIds = stores.map(store => store.id).filter(id => !!id);
    const query = `?matchings=${storesIds.join(',')}&from_date=${fromDate}`
    const { data } = await axios.get(`${domain}/api/v1/campaigns/1026/price_history/${query}`);
    productStore.saveStoresPriceHistory(data.objects);
  } catch (error) {
    console.log(error);
  }

}