chrome.browserAction.onClicked.addListener((tab) => {

  // dev: async fetch bundle
  fetch(`http://localhost:3000/js/inject.bundle.js`)
    .then(res => res.text())
    .then((fetchRes) => {
      chrome.tabs.executeScript(tab.id, { code: fetchRes, runAt: 'document_end' }, () => {});
    });
});