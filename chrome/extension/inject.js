import React, { Component } from 'react';
import ReactDom from 'react-dom';
import Dock from 'react-dock';

const toggler = document.getElementById('pi_toggler');

if (toggler) {
  toggler.click();
} else {
  class InjectApp extends Component {
    constructor(props) {
      super(props);
      this.state = { isVisible: true };
    }

    buttonOnClick = () => {
      this.setState({ isVisible: !this.state.isVisible });
    };

    render() {
      return (
        <div>
          <button onClick={this.buttonOnClick} id={'pi_toggler'} style={{display: 'none'}}>
            open pi extension
          </button>
          <Dock
            position="right"
            dimMode="transparent"
            fluid={false}
            size={500}
            isVisible={this.state.isVisible}
          >
            <iframe
              style={{
                width: '100%',
                height: '100%',
              }}
              frameBorder={0}
              allowTransparency
              src={chrome.extension.getURL(`inject.html?protocol=${location.protocol}`)}
            />
          </Dock>
        </div>
      );
    }
  }

  const injectDOM = document.createElement('div');
  injectDOM.id = 'inject_competera_pi';
  document.body.appendChild(injectDOM);
  ReactDom.render(<InjectApp />, injectDOM);
}

